package com.example.todoapp.repositories;
import java.util.Random;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;

import com.example.todoapp.models.ScanFilePath;
import com.example.todoapp.models.Scanner;
import com.example.todoapp.models.Todo;

public class ScannerService{
	
	public static ArrayList<Todo> retrieveConfiguration(Scanner scanner, String gitPath)
	{
		
		String localPath=cloneGitUrl(gitPath);
		scanner.setPath(localPath);
		ArrayList<Todo> todolist= fetchConfiguration(scanner);		
		return todolist;		
	}
	
	public static String cloneGitUrl(String gitPath) 
	{
		Random random = new Random();
		int randomPath = random.nextInt(1000);
		String localPath="c:\\gitProject\\"+String.valueOf(randomPath);
		
		try {
			Git.cloneRepository()
					  .setURI(gitPath)				
					  .setDirectory(new File(localPath))
					  .setCloneAllBranches( true )
					  .call();
		} catch (InvalidRemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.eclipse.jgit.api.errors.TransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return localPath;
		
	}
	
	public static ArrayList<Todo> fetchConfiguration(Scanner scanner)
	{
		String path=scanner.getPath();
		System.out.println("what is local path:"+path);
		//String path="c:\\gitProject";
		Map<String,List<String>> scanType=scanner.getScanType();
		ArrayList<Todo> todolist= new ArrayList<Todo>();
		
		scanType.forEach((k,v) -> {
			//HashMap<String,List<String>> hm =new HashMap<String,List<String>>();
			
			List<ScanFilePath> scanFilePathlist= new ArrayList<ScanFilePath>();
		try {
			Files.walk(Paths.get(path))		
				.filter(Files::isRegularFile)
				.forEach(n->{
					try {
						List<String> fileList= new ArrayList<String>();				
						fileList.addAll(Files.lines(Paths.get(n.toString()),Charset.forName("ISO_8859_1") )
						.filter(line->v.stream().anyMatch(val->line.toLowerCase().contains(val.toLowerCase())))
						.collect(Collectors.toList()));
						
						Predicate<List<String>> pSize=arr-> arr.size()>0;
						
						if(pSize.test(fileList))
						{
							ScanFilePath scanFilePath=new ScanFilePath("Searched File Path: "+n.toString(),fileList);
							scanFilePathlist.add(scanFilePath);
						}
						//hm.put(n.toString(),fileList);
												
					} catch (IOException e) {
						e.printStackTrace();
					}					
				});
				} catch(IOException ioe ){}
			//map.put(k,hm);
			//Todo todo = new Todo(k,hm);
			Todo todo = new Todo(k,scanFilePathlist);
			todolist.add(todo);
		});
		return todolist;
	}

}
