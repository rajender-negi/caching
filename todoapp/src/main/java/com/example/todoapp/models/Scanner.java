package com.example.todoapp.models;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class Scanner {
    public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	private List<String> includeFileTypeList;
    private String path;
    public List<String> getIncludeFileTypeList() {
		return includeFileTypeList;
	}
	public void setIncludeFileTypeList(List<String> includeFileTypeList) {
		this.includeFileTypeList = includeFileTypeList;
	}
	public List<String> getExcludeFileTypeList() {
		return excludeFileTypeList;
	}
	public void setExcludeFileTypeList(List<String> excludeFileTypeList) {
		this.excludeFileTypeList = excludeFileTypeList;
	}
	public Map<String, List<String>> getScanType() {
		return scanType;
	}
	public void setScanType(Map<String, List<String>> scanType) {
		this.scanType = scanType;
	}
	private List<String> excludeFileTypeList;
    private Map<String, List<String>> scanType;
         
}