package com.example.todoapp.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.todoapp.models.Scanner;
import com.example.todoapp.models.Todo;
import com.example.todoapp.repositories.ScannerService;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class TodoController {

/*    @Autowired
    TodoRepository todoRepository;*/

	@Autowired
    private Scanner scanner;

    /*@GetMapping("/todos")
    public List<Todo> getAllTodos() {
        Sort sortByCreatedAtDesc = new Sort(Sort.Direction.DESC, "createdAt");
        return todoRepository.findAll(sortByCreatedAtDesc);
    }*/

	
	
    @GetMapping("/initialtodos")
    public List<Todo> intialCachingSetup() {
    	List<Todo> tlist=new ArrayList<Todo>();	
    	deleteDirectory(new File("c://gitProject"));
    	return tlist;    	    	
    }

    boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }
    /*@GetMapping("/todos")
    public List<Todo> cachingScanner() {
    	String gitPath="";
    	String localPath="";
    	List<Todo> tlist=new ArrayList<Todo>();	
    	
    	tlist=ScannerService.retrieveConfiguration(scanner,gitPath, localPath);
    	
    	return tlist;    	    	
    }*/

    @PostMapping("/todos")
    public List<Todo> cachingScanner(@Valid @RequestBody Todo todo) {
    	List<Todo> tlist=new ArrayList<Todo>();
    	String gitPath=todo.getGitPath();
    	if(gitPath!=null && gitPath.length()>0)
    	{
    	tlist=ScannerService.retrieveConfiguration(scanner,todo.getGitPath());
    	}
    	
    	return tlist;    	    	
    }
    /*@GetMapping("/todos")
    public List<Todo> cachingScanner() {
    	List<Todo> tlist=new ArrayList<Todo>();	
       Todo t=new Todo();
       t.setTitle("raj");
       tlist.add(t);
       
       Todo t1=new Todo();
       t1.setTitle("aaj");
       tlist.add(t1);
       
    	return tlist;    	    	
    }*/

  /*  @PostMapping("/todos")
    public Todo createTodo(@Valid @RequestBody Todo todo) {
        todo.setCompleted(false);
        return todoRepository.save(todo);
    }

    @GetMapping(value="/todos/{id}")
    public ResponseEntity<Todo> getTodoById(@PathVariable("id") String id) {
        return todoRepository.findById(id)
                .map(todo -> ResponseEntity.ok().body(todo))
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping(value="/todos/{id}")
    public ResponseEntity<Todo> updateTodo(@PathVariable("id") String id,
                                           @Valid @RequestBody Todo todo) {
        return todoRepository.findById(id)
                .map(todoData -> {
                    todoData.setTitle(todo.getTitle());
                    todoData.setCompleted(todo.getCompleted());
                    Todo updatedTodo = todoRepository.save(todoData);
                    return ResponseEntity.ok().body(updatedTodo);
                }).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping(value="/todos/{id}")
    public ResponseEntity<?> deleteTodo(@PathVariable("id") String id) {
        return todoRepository.findById(id)
                .map(todo -> {
                    todoRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }*/
}

//https://www.callicoder.com/spring-boot-mongodb-angular-js-rest-api-tutorial/