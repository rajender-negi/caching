package com.example.todoapp.models;

import java.util.List;

public class ScanFilePath {
	private String filename;
	private List<String> component;
	
	public ScanFilePath(String filename, List<String> component)	
	{
		this.filename=filename;
		this.component=component;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public List<String> getComponent() {
		return component;
	}

	public void setComponent(List<String> component) {
		this.component = component;
	}
}
