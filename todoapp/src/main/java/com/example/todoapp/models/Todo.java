package com.example.todoapp.models;

import java.util.HashMap;
import java.util.List;

/*import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;*/

//@Document(collection="todos")
//@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class Todo {
    /*@Id
    private String id;

    @NotBlank
    @Size(max=100)
    @Indexed(unique=true)
    private String title;

    private Boolean completed = false;

    private Date createdAt = new Date();

    public Todo() {
        super();
    }

    public Todo(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return String.format(
                "Todo[id=%s, title='%s', completed='%s']",
                id, title, completed);
    }
	
    public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	private List<String> includeFileTypeList;
    private String path;
    public List<String> getIncludeFileTypeList() {
		return includeFileTypeList;
	}
	public void setIncludeFileTypeList(List<String> includeFileTypeList) {
		this.includeFileTypeList = includeFileTypeList;
	}
	public List<String> getExcludeFileTypeList() {
		return excludeFileTypeList;
	}
	public void setExcludeFileTypeList(List<String> excludeFileTypeList) {
		this.excludeFileTypeList = excludeFileTypeList;
	}
	public Map<String, List<String>> getScanType() {
		return scanType;
	}
	public void setScanType(Map<String, List<String>> scanType) {
		this.scanType = scanType;
	}
	private List<String> excludeFileTypeList;
    private Map<String, List<String>> scanType;
    
    private String id;

     private String title;

    private Boolean completed = false;

    private Date createdAt = new Date();

     public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
*/
private String gitPath;
public String getGitPath() {
	return gitPath;
}
public void setGitPath(String gitPath) {
	this.gitPath = gitPath;
}
public String getLocalPath() {
	return localPath;
}
public void setLocalPath(String localPath) {
	this.localPath = localPath;
}
private String localPath;
private String scankey;
public String getScankey() {
	return scankey;
}
public void setScankey(String scankey) {
	this.scankey = scankey;
}
public List<ScanFilePath> getScanvalue() {
	return scanvalue;
}
public void setScanvalue(List<ScanFilePath> scanvalue) {
	this.scanvalue = scanvalue;
}
private List<ScanFilePath> scanvalue; 

public Todo(String scankey, List<ScanFilePath> scanvalue)
{
 this.scankey=scankey;
 this.scanvalue=scanvalue;
}
}