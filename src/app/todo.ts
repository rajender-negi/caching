import { ScanFilePath } from './scanFilePath';

export class Todo {
  id: string;
  title: string;
  gitPath: string;
  completed: boolean;
  createdAt: Date;
  path: string;
  scankey: string;
  scanvalue: ScanFilePath[];
    
}


